// People Are Trees - Panel 1 (Sine Wave Panel 1) Arduino Code
// Author: Jacob Faunce
// Date: 04/17/2018

// Arduino Nano / Uno R3
// PINS
// Speaker on D5
// Photoresistor Array on A0

// Includes / Initializations
#include "Volume.h"
Volume vol;

// Declarations
// Argument which determines distance at which the lights are turned on
// Tune by printing analogRead(A0) to serial monitor
int lightThreshold = 700;

// Pins
const int speakerPin = 5;
const int analogInPin = A0;

// Setup
void setup()
{
  vol.begin();
  pinMode(13, OUTPUT);

}

// Loop
void loop()
{
  // Generate Volume
  float volume = (map(analogRead(A0), 900, 0, 0, 100)) * 3;
  // Generate Pitch
  float pitch = (map(analogRead(A0), 900, 0, 0, 300)) * 3;
  // Speaker Output
  vol.tone(pitch, (255 * (1 - volume)));

  // Drive lights
  if (analogRead(A0) < lightThreshold) {
    digitalWrite(13, HIGH);
  }
  else {
    digitalWrite(13, LOW);
  }
}

