// People Are Trees - Panel 5 (Gate Panel) Arduino Code
// Author: Jacob Faunce
// Date: 04/17/2018

// Arduino Nano / Uno R3
// PINS
// Speaker on D5
// Photoresistor Array on A0


// Includes / Initializations
#include "Volume.h"
Volume vol;
int freq;

// Argument which determines distance at which the "gate" is opened
// Tune by printing analogRead(A0) to serial monitor
int soundThreshhold = 700;

// Counting Variables
int counter = 0;
int oneSecond = 156;
int oneMinute = oneSecond * 60;

// Setup
void setup() {
  vol.begin();
  pinMode(13, OUTPUT);
}

// Loop
void loop() {
  // Generate noise values
  float bound = (map(analogRead(A0), 0, 600, 0, 100)) / 100.00;
  freq = random(94000 * bound, 96000 * bound);

  // Determine gate state
  if (analogRead(A0) < soundThreshhold) {
    // Output noise
    vol.tone(freq, 255 * (1));
    vol.delay(1);
    vol.noTone();
    vol.tone(freq, 0);
    vol.delay(1);
  }

  // Increment cycle counter
  // COUNTER CALCULATIONS ARE BASED ON LOGIC UP TO THIS POINT, IF MOVED, TIMING WILL BE INNACURATE
  counter++;

  // "Random" interval to light up LED strip
  // Program runs at about 158 cycles / second
  // Currently LED is triggered roughly every 12 minutes for 10 seconds

  // Wait 12 minutes
  if (counter > oneMinute * 12) {
    // Turn on
    digitalWrite(13, HIGH);

  }
  // Wait 10 seconds
  if (counter > (oneMinute * 12) + (oneSecond * 10)) {
    // Turn off
    digitalWrite(13, LOW);
    // Reset counter
    counter = 0;
  }

}
