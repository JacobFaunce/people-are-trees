// People Are Trees - Panel 3 (Sawtooth Wave Panel) Arduino Code
// Author: Jacob Faunce
// Date: 04/17/2018

// Arduino Nano / Uno R3
// PINS
// Speaker on D9
// Photoresistor Array on A0


// Declarations
// Argument which determines distance at which the lights are turned on
// Tune by printing analogRead(A0) to serial monitor
int lightThreshold = 700;
//Volume (0 to 255)
byte volume = 255;

// Includes / Initializations
#include <MozziGuts.h>
#include <Oscil.h> // oscillator template
#include <tables/square_no_alias_2048_int8.h> // sine table for oscillator
#include <EventDelay.h>
Oscil <SQUARE_NO_ALIAS_2048_NUM_CELLS, AUDIO_RATE> aSin(SQUARE_NO_ALIAS_2048_DATA);

#define CONTROL_RATE 64

void setup() {
  startMozzi(CONTROL_RATE); // set a control rate of 64 (powers of 2 please)
}


void updateControl() {
}


int updateAudio() {
  return ((int)aSin.next() * volume) >> 8; // return an int signal centred around 0
}


void loop() {
  EventDelay(10);
  float pitch = (map(analogRead(A0), 900, 0, 900, 0)) * 3;
  aSin.setFreq(pitch);
  audioHook(); // required here

  // Drive lights
  if (analogRead(A0) < lightThreshold) {
    digitalWrite(13, HIGH);
  }
  else {
    digitalWrite(13, LOW);
  }
}



